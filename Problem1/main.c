#include <stdio.h>
#include <stdlib.h>

struct date {
    int day;
    int month;
    int year;
};

struct machine {
    int name;
    char *memory;
};

struct time {
    int *day;
    int *month;
    int *year;
};

int main() {
    struct date *dates = malloc(sizeof(struct date));
    struct machine *mpu641 = malloc(sizeof(struct machine));
    struct time *times = malloc(sizeof(struct time));
    struct time *sample[10];
    char *CPUtype="cputype";

    struct date *pd = dates;
    struct machine *pm = mpu641;
    struct time *pt = times;
    struct time **ps = sample;

    dates->day = 10;
    mpu641->memory = CPUtype;
    mpu641->name = 10;
    times->day = (int *)malloc(sizeof(int));
    *(times->day) = 10;
    sample[2] = (struct time *)malloc(sizeof(struct time));
    sample[2]->month = (int *)malloc(sizeof(int));
    *(sample[2]->month) = 12;

    printf("dates:\n");
    printf("day=%d\n", dates->day);
    printf("pointer of dates:\n");
    printf("day=%d\n", pd->day);

    printf("mpu641:\n");
    printf("name=%d memory=%s\n", mpu641->name, mpu641->memory);
    printf("pointer of mpu641:\n");
    printf("name=%d memory=%s\n", pm->name, pm->memory);

    printf("times:\n");
    printf("day=%d\n", *(times->day));
    printf("pointer of times:\n");
    printf("day=%d\n", *(pt->day));

    printf("sample[%d]:\n", 2);
    printf("month=%d\n", *(sample[2]->month));
    printf("pointer of times:\n");
    printf("month=%d\n", *(ps[2]->month));

    free(dates);
    free(mpu641);
    free(times);
    free(sample);
    return 0;
}