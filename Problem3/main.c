#include <stdio.h>
#include <stdlib.h>

struct item {
    int age;
    struct item *next;
};

struct item *createItem() {
    int length = 10;
    struct item *p = malloc(sizeof(struct item));
    struct item *start = p;

    printf("Input %d ages:\n", length);
    for(int i=0; i<length; i++) {
        scanf("%d", &(p->age));
        p->next = malloc(sizeof(struct item));
        p = p->next;
    }
    p = NULL;

    return start;
}

int main() {
    struct item *p = createItem();
    printf("Inputted Ages:\n");
    for (int i=0; i<10; i++) {
        printf("%d ",p->age);
        p = p->next;
    }
    printf("\n");
    return 0;
}