#include <stdio.h>
#include <stdlib.h>

struct point {
    int x;
    int y;
};

void printPoint(struct point *p) {
    printf("[x, y] = %d %d\n", p->x, p->y);
}

double average(struct point *p, int length, char point) {
    int sum = 0;
    if (point == 'x') {
        for (int i=0; i<length; i++) {
            sum += (p+i)->x;
        }
    } else if (point == 'y'){
        for (int i=0; i<length; i++) {
            sum += (p+i)->y;
        }
    }
    return (double)sum/(double)length;
}

struct point *vectorAverage(struct point *p, int length) {
    double avgx = average(p, length, 'x');
    double avgy = average(p, length, 'y');
    struct point *avg = malloc(sizeof(struct point));
    avg->x = (int)avgx;
    avg->y = (int)avgy;

    return avg;
}

int main() {
    int count = 5;
    struct point *points = malloc(sizeof(struct point)*count);
    struct point *avgpoint;
    for (int i=0; i<count; i++) {
        printf("Input x and y:\n");
        scanf("%d %d", &(points[i].x), &(points[i].y));
    }
    avgpoint = vectorAverage(points, count);
    printf("Average point:\n");
    printPoint(avgpoint);
    return 0;
}